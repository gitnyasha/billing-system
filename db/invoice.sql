-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 19, 2021 at 09:29 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) NOT NULL,
  `invoice_no` bigint(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `due` date DEFAULT NULL,
  `date_paid` date NOT NULL DEFAULT '0000-00-00',
  `phone` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `membership` varchar(255) DEFAULT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `vat` decimal(10,0) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `invoice_no`, `email`, `company`, `address`, `due`, `date_paid`, `phone`, `status`, `membership`, `discount`, `vat`, `created_at`, `updated_at`, `firstname`, `lastname`, `user_id`) VALUES
(5, 2, 'marshall.chikari@gmail.com', 'DoucetechZw', '2469 Southlea Park Harare', '2021-06-19', '2021-06-19', '+263773662924', 'unpaid', 'bronze', NULL, NULL, '2021-06-19 16:18:53.000000', '2021-06-19 16:18:53.000000', 'Marshall', 'Chikari', 2),
(6, 2, 'fulamos@gmail.com', 'frferf', '2469 Southlea Park Harare', '2021-06-19', '2021-06-21', '', 'paid', 'gold', NULL, NULL, '2021-06-19 17:22:42.000000', '2021-06-19 17:22:42.000000', 'Marshall', 'Chikari', 2),
(7, 2, 'fulamos@gmail.com', 'frferf', '2469 Southlea Park Harare', '2021-06-19', '2021-06-21', '', 'paid', 'gold', NULL, NULL, '2021-06-19 17:22:49.000000', '2021-06-19 17:22:49.000000', 'Marshall', 'Chikari', 2),
(8, 2, 'charlochikari@gmail.com', 'DoucetechZw', '2469 Southlea Park Harare', '2021-06-03', '2021-06-10', '', 'created', 'bronze', NULL, NULL, '2021-06-19 17:24:39.000000', '2021-06-19 17:24:39.000000', 'Charlo', 'Chikari', 4),
(9, 2, 'fulamos@gmail.com', 'frferf', '2469 Southlea Park Harare', '2021-06-24', '2021-06-24', '', 'pending', 'platinum', NULL, NULL, '2021-06-19 17:25:46.000000', '2021-06-19 17:25:46.000000', 'Marshall', 'Chikari', 2),
(10, 2, 'charlochikari@gmail.com', 'DoucetechZw', '2469 Southlea Park Harare', '2021-06-24', '2021-06-29', '', 'paid', 'gold', NULL, NULL, '2021-06-19 17:31:57.000000', '2021-06-19 17:31:57.000000', 'Charlo', 'Chikari', 4),
(11, 2, 'tawandachikari56@gmail.com', 'logistics', '2469 Southlea Park Harare', '2021-06-19', '2021-06-23', '', 'created', 'bronze', NULL, NULL, '2021-06-19 17:42:59.000000', '2021-06-19 17:42:59.000000', 'Tawanda', 'Chikari', 3),
(12, 2, 'tawandachikari56@gmail.com', 'logistics', '2469 Southlea Park Harare', '2021-06-24', '2021-06-22', '', 'unpaid', 'gold', NULL, NULL, '2021-06-19 17:45:39.000000', '2021-06-19 17:45:39.000000', 'Tawanda', 'Chikari', 3),
(13, 2, 'charlochikari@gmail.com', 'DoucetechZw', '2469 Southlea Park Harare', '2021-06-24', '2021-06-29', '', 'created', 'bronze', NULL, NULL, '2021-06-19 17:51:29.000000', '2021-06-19 17:51:29.000000', 'Charlo', 'Chikari', 4);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `grade` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `firstname`, `address`, `city`, `lastname`, `company`, `email`, `grade`, `created_at`, `updated_at`) VALUES
(2, 'Marshall', '2469 Southlea Park Harare', 'harare', 'Chikari', 'frferf', 'fulamos@gmail.com', 'bronze', '2021-06-14 12:42:35', '2021-06-14 12:42:35'),
(3, 'Tawanda', '2469 Southlea Park Harare', 'Harare', 'Chikari', 'logistics', 'tawandachikari56@gmail.com', 'platinum', '2021-06-14 12:42:51', '2021-06-14 12:42:51'),
(4, 'Charlo', '2469 Southlea Park Harare', 'harare', 'Chikari', 'DoucetechZw', 'charlochikari@gmail.com', 'bronze', '2021-06-19 11:12:40', '2021-06-19 11:12:40');

-- --------------------------------------------------------

--
-- Table structure for table `tasklog`
--

CREATE TABLE `tasklog` (
  `id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tasklog`
--

INSERT INTO `tasklog` (`id`, `created`) VALUES
(1, '2021-06-11 14:33:32'),
(2, '2021-06-11 14:33:46'),
(3, '2021-06-12 13:28:45'),
(4, '2021-06-12 13:29:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasklog`
--
ALTER TABLE `tasklog`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tasklog`
--
ALTER TABLE `tasklog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `members` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
