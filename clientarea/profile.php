<?php
 if ( isset( $_GET[ "client" ] ) ) {
  $id = $_GET[ "client" ];
}

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "invoice";

$conn = new PDO("mysql:host=localhost;dbname=invoice", "root", "");

function fetch_customer_data($conn, $id)
{
  $stmt = $conn->prepare("SELECT * FROM members WHERE id=:id");
  $stmt->execute(['id' => $id]); 
  while ($row = $stmt->fetch()) {
      echo "" .$row["firstname"].' '.$row["lastname"]. "";
  }
}

function fetch_client_invoices($conn, $id) {
  $query = "SELECT * FROM invoices WHERE user_id='$id' ORDER BY created_at DESC";
  $statement = $conn->prepare($query);
  $statement->execute();
  $result = $statement->fetchAll();
  $output = '
  <div class="table-responsive">
      <table class="table table-striped table-sm">
          <thead>
          <tr>
              <th>#</th>
              <th>Name</th>
              <th>Company</th>
              <th>Due Date</th>
              <th>Date Paid</th>
              <th>Membership</th>
              <th>Status</th>
          </tr>
          </thead>
          <tbody>
  ';
  foreach($result as $row)
  {
  $output .= '
  <tr>
      <td>'.$row["invoice_no"].'</td>
      <td>'.$row["firstname"].' '.$row["lastname"].'</td>
      <td>'.$row["company"].'</td>
      <td>'.$row["due"].'</td>
      <td>'.$row["date_paid"].'</td>
      <td>'.$row["membership"].'</td>
      <td><p class='.$row["status"].'>'.$row["status"].'</p></td>
  </tr>
  ';
  }
  $output .= '
      </tbody>
  </table>
  </div>
  ';
  return $output;
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Members</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Company name</a>
      <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
            <li class="nav-item">
            <a class="nav-link active" href="#">
                <span data-feather="home"></span>
                <?php echo fetch_customer_data($conn, $id) ?>
               <span class="sr-only">(current)</span>
            </a>
            </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">Invoices</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-info">Download PDF</button>
              </div>
            </div>
          </div>
          <?php echo fetch_client_invoices($conn, $id) ?>
        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
  </body>
</html>
