<?php
try {
    $host = "localhost";
    $dbname = "invoice";
    $user = "root";
    $pass = "";

    # MySQL with PDO_MYSQL
    $db = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

    $stmt = $db->prepare("INSERT INTO tasklog(`created`) VALUES(NOW())");
    $stmt->execute();
  
    $db = null;
}
catch(PDOException $e) {
    echo $e->getMessage();
}