<?php

try {
    if (isset($_POST["action"])) {
        $firstname = $_POST["firstname"];
        $lastname = $_POST["lastname"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $address = $_POST["address"];
        $company = $_POST["company"];
        $due = $_POST["due"];
        $status = $_POST["status"];
        $date_paid = $_POST["datepaid"];
        $membership = $_POST["membership"];

        $host = "localhost";
        $dbname = "invoice";
        $user = "root";
        $pass = "";

        # MySQL with PDO_MYSQL
        $db = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $db->prepare("INSERT INTO invoices(
            `invoice_no`,
            `firstname`, 
            `lastname`, 
            `email`, 
            `company`, 
            `address`, 
            `phone`, 
            `due`, 
            `status`, 
            `date_paid`, 
            `membership`, 
            `created_at`, 
            `updated_at`) VALUES(
                '2',
                '$firstname',
                '$lastname',
                '$email',
                '$company',
                '$address',
                '$phone',
                '$due',
                '$status',
                '$date_paid',
                '$membership',
                NOW(),
                NOW()
            )");
        $stmt->execute();

        $db = null;
    }
} catch(PDOException $e) {
    echo $e->getMessage();
}

?>
<form method="post">
  <div class="row">
      <div class="col-lg-6">
          <label for="firstname">First Name</label>
          <input type="text" name="firstname" class="form-control" id="firstname" required >
      </div>
      <div class="col-lg-6">
          <label for="lastname">Last Name</label>
          <input type="text" name="lastname" class="form-control" id="lastname" required >
      </div>
  </div>
  <hr>
  <div class="row">
      <div class="col-lg-6">
          <label for="name">Company / Organisation</label>
          <input type="text" name="company" class="form-control" id="company" required >
      </div>
      <div class="col-lg-6">
      <label for="email">Email</label>
      <input type="email" name="email" id="email" required class="form-control">
      </div>
  </div>
  <hr>
  <div class="row">
      <div class="col-lg-6">
          <label for="address">Address</label>
          <input type="text" name="address" class="form-control" id="address" >
          <small class="text-muted">Optional</small>
      </div>
      <div class="col-lg-6">
          <label for="phone">Phone</label>
          <input type="text" name="phone" id="phone" class="form-control">
      </div>
  </div>
  <hr>
  <div class="row">
      <div class="col-lg-6">
          <label for="due">Due Date</label>
          <input type="date" name="due" id="due" class="form-control" required >
      </div>
      <div class="col-lg-6">
          <label for="datepaid">Date Paid</label>
          <input type="date" name="datepaid" class="form-control" id="date-paid">
          <small class="text-muted">Optional</small>
      </div>
  </div>
  <hr>
  <div class="row">
      <div class="col-lg-6">
          <label for="membership">Type of Membership</label>
          <select name="membership" id="membership" class="form-control">
              <option value="bronze">Bronze $5</option>
              <option value="silver">Silver $10</option>
              <option value="gold">Gold $15</option>
              <option value="platinum">Platinum $20</option>
          </select>
      </div>
      <div class="col-lg-6">
          <label for="status">Invoice Status</label>
          <select name="status" id="status" class="form-control">
              <option value="created">Created</option>
              <option value="pending">Pending</option>
              <option value="unpaid">unpaid</option>
              <option value="paid">Paid</option>
          </select>
      </div>
  </div>
  <hr>
  <input type="submit" name="action" class="btn btn-primary" value="Create Invoice" />
</form>