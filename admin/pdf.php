<?php
$connect = new PDO("mysql:host=localhost;dbname=invoice", "root", "");

require '../vendor/autoload.php';
require 'config.php';

use Dompdf\Dompdf;

if ( isset( $_POST[ "invoiceid" ] ) ) {
  $id = $_REQUEST['invoiceid'];
}

if(isset($_POST["download_pdf"]))
{

  $show = send_invoices_data($connect);
  $dompdf = new Dompdf();
  $dompdf->loadHtml($show);
  $dompdf->setPaper('A4', 'landscape');
  $dompdf->render();
  $dompdf->stream('invoice.pdf', [
    "Attachment" => true
]);
}

if(isset($_POST["members_pdf"]))
{

  $show = fetch_customer_pdf($connect);
  $dompdf = new Dompdf();
  $dompdf->loadHtml($show);
  $dompdf->setPaper('A4', 'landscape');
  $dompdf->render();
  $dompdf->stream('invoice.pdf', [
    "Attachment" => false
]);
}

if(isset($_POST["view_pdf"]))
{

  $show = send_invoices_data($connect);
  $dompdf = new Dompdf();
  $dompdf->loadHtml($show);
  $dompdf->setPaper('A4', 'landscape');
  $dompdf->render();
  $dompdf->stream('invoice.pdf', [
        "Attachment" => false
    ]);
}

if(isset($_POST["single_invoice_pdf"]))
{

  $show = pdf_invoice_details($connect, $id);
  $dompdf = new Dompdf();
  $dompdf->loadHtml($show);
  $dompdf->setPaper('A4', 'landscape');
  $dompdf->render();
  $dompdf->stream('invoice.pdf', [
        "Attachment" => false
    ]);
}