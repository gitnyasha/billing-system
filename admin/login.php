<?php
   include("config.php");
   session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <title>Login</title>
</head>
<body>
    <div class="container pt-5">
        <div class="row justify-center pt-5">
            <div class="col-lg-6 mx-auto pt-3">
            <h1>Admin Login</h1>
             <form action="check_login.php" method="post">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" name="username" class="form-control" id="username" required >
                </div>

                <div class="form-group">
                    <label for="username">Password</label>
                    <input type="password" name="password" class="form-control" id="password" required >
                </div>
                <button class="btn btn-primary" name="submit" type="submit">Login</button>
             </form>
            </div>
        </div>
    </div>
</body>
</html>