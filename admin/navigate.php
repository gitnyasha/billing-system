<li class="nav-item">
<a class="nav-link active" href="../admin/index.php">
    <span data-feather="home"></span>
    Dashboard <span class="sr-only">(current)</span>
</a>
</li>
<li class="nav-item">
<a class="nav-link" href="../admin/invoices.php">
    <span data-feather="shopping-cart"></span>
    All Invoices
</a>
</li>
<li class="nav-item">
<a class="nav-link" href="../admin/users.php">
    <span data-feather="users"></span>
    All Clients
</a>
</li>
<li class="nav-item">
<a class="nav-link" href="../admin/reports.php">
    <span data-feather="bar-chart-2"></span>
    Reports
</a>
</li>
