<?php
   
   $connect = new PDO("mysql:host=localhost;dbname=invoice", "root", "");

   // show single invoice in pdf format
   function pdf_invoice_details($connect, $id)
   {
        $stmt = $connect->prepare("SELECT * FROM invoices WHERE id=:id");
        $stmt->execute(['id' => $id]); 
        while ($row = $stmt->fetch()) {
         $newtime = strtotime($row["created_at"]);
         $stmt->time = date('Y-m-d',$newtime);
         $output = '
         <!DOCTYPE html>
         <html lang="en">
         <head>
            <style>
            .invoice-box {
               max-width: 80%;
               max-height: 100%;
               margin: auto;
               border: 1px solid #eee;
               box-shadow: 0 0 5px rgba(0, 0, 0, .10);
               font-size: 14px;
               line-height: 24px;
               font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
               color: #555;
            }
            
            .resize {
               height: 120px;
               width: 200px;
            }
            
            .invoice-box table {
               width: 100%;
               line-height: inherit;
               text-align: left;
            }
            
            .invoice-box table td {
               padding: 5px;
               vertical-align: middle;
            }
            
            .invoice-box table tr td:nth-child(2) {
               text-align: right;
            }

            .unpaid {
               background-color: red;
               color: #fff;
               padding: 5px;
               border-radius: 5px;
            }

            .paid {
              background-color: rgb(26, 172, 104);
              color: #fff;
               padding: 5px;
               border-radius: 5px;
            }

            .pl-10 {
               text-align: left;
            }
            
            .right {
               text-align: right;
            }

            .center {
               text-align: center;
            }
            
            .invoice-box table tr.top table td {
               padding-bottom: 20px;
            }
            
            .invoice-box table tr.top table td.title {
               font-size: 45px;
               line-height: 45px;
               color: #333;
            }
            
            .invoice-box table tr.information table td {
               padding-bottom: 40px;
            }
            
            .invoice-box table tr.heading td {
               background: #eee;
               border-bottom: 1px solid #ddd;
               font-weight: bold;
            }
            
            .invoice-box table tr.details td {
               padding-bottom: 20px;
            }
            
            .invoice-box table tr.item td{
               border-bottom: 1px solid #eee;
            }
            
            .invoice-box table tr.item.last td {
               border-bottom: none;
            }
            
            
            .invoice-box table tr.total td:nth-child(2) {
               border-top: 2px solid #eee;
               font-weight: bold;
            }

            .btn {
               display: none;
            }
            </style>
         </head>
         <body>
         ';
         $output .= '
         <div class="invoice-box">
            <table cellpadding="0" cellspacing="0">
               <tr class="top">
               <td colspan="4">
                  <table>
                     <tr>
                     <td class="title">
                        logo
                     </td>

                     <td>
                        Invoice #: <b>'.$row["invoice_no"].'</b>
                        <br>
                        Invoice Date: <b>'.$stmt->time.'</b>
                        <br>
                        Due: <b>'.$row["due"].'</b>
                        <br>
                        Status: <span class="'.$row["status"].'">'.$row["status"].'</span>
                     </td>
                     </tr>
                  </table>
               </td>
               </tr>

               <tr class="information">
               <td colspan="4">
                  <table>
                     <tr>
                     <td>
                        <b>Hekani</b><br>
                        1234 road harare<br>
                        <i class="fa fa-envelope"></i> example@mail.com<br>
                        <i class="fa fa-phone"></i> 07123456789
                     </td>

                     <td>
                        <b>Customer</b><br>
                        '.$row["address"].'<br>
                        <i class="far fa-envelope"></i> '.$row["email"].'<br>
                        <i class="fas fa-phone"></i> '.$row["phone"].'
                     </td>
                     </tr>
                  </table>
               </td>
               </tr>

               <tr class="heading">
               <td class="pl-10">Payment Method</td>
               <td colspan="2"></td>
               <td class="center">Cache</td>
               </tr>

               <tr class="details">
               <td class="pl-10">Paynow Payment Gateway</td>
               <td colspan="2"></td>
               <td class="center"><button class="btn btn-sm btn-outline-secondary">Make Payment</button></td>
               </tr>

               <tr class="heading">
               <td class="pl-10">Item</td>
               <td colspan="1"></td>
               <td class="center">Qty</td>
               <td class="center">Total</td>
               </tr>
         ';
         $query = "SELECT * FROM invoiceitems WHERE invoiceid='$id'";
         $statement = $connect->prepare($query);
         $statement->execute();
         $result = $statement->fetchAll();
         $total = 0;
         $item_total = 0;
         foreach($result as $rows)
         {
         $item_total = $rows['quantity'] * $rows['amount'];
         $total += $item_total;
         $output .= '
               <tr class="item">
                  <td class="pl-10">
                     '.$rows["description"].'
                  </td>

                  <td colspan="1"></td>

                  <td class="center">
                     x '.$rows["quantity"].'
                  </td>

                  <td class="center">
                  '.$item_total.'
                  </td>
               </tr>

               <tr>
               <td colspan="4"></td>
               </tr>
         ';
         }
         $output .= '
         <tr>
               <td colspan="2"></td>
               <td><b>Subtotal</b></td>
               <td class="center">'.$total.'</td>
               </tr>

               <tr>
               <td colspan="2"></td>
               <td><b>Discount 10%</b></td>
               <td class="center">- $0.00</td>
               </tr>

               <tr>
               <td colspan="2"></td>
               <td><b>VAT 10%</b></td>
               <td class="center">+ $0.00</td>
               </tr>

               <tr class="heading">
               <td colspan="2"></td>
               <td><b>TOTAL:</b></td>
               <td class="center">
                  <b>$'.$total.'</b>
               </td>
            </tr>
         ';
         $output .= '
         </table>
         </div>
         </body>
        </html>
         ';
         return $output;
        }
   }

   // Get all invoices
   function fetch_invoice_data($connect)
      {
      $query = "SELECT * FROM invoices ORDER BY created_at DESC";
      $statement = $connect->prepare($query);
      $statement->execute();
      $result = $statement->fetchAll();
      $output = '
      <div class="table-responsive">
         <table class="table table-striped table-sm">
            <thead>
            <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Company</th>
                  <th>Due Date</th>
                  <th>Date Paid</th>
                  <th>Status</th>
                  <th>View</th>
                  <th>PDF</th>
                  <th>Reminder</th>
            </tr>
            </thead>
            <tbody>
      ';
      foreach($result as $row)
      {
      $output .= '
         <tr>
         <td>'.$row["invoice_no"].'</td>
         <td><a href="../admin/profile.php?client=' . $row["user_id"] . '">' . $row["firstname"] . ' ' . $row["lastname"] . '</a></td>
         <td>'.$row["company"].'</td>
         <td>'.$row["due"].'</td>
         <td>'.$row["date_paid"].'</td>
         <td><a class="'.$row["status"].'">'.$row["status"].'</a></td>
         <td><a href="../admin/invoice.php?invoice=' . $row["id"] . '"><i class="fa fa-eye" style="font-size:24px;color:#222"></i></a></td>
         <td>
         <form action="pdf.php" method="post">
            <input type="hidden" name="invoiceid" value="' . $row["id"] . '" />
            <button type="submit" name="single_invoice_pdf"><i class="fa fa-download"></i></button>
         </form>
         </td>
         <td>
         <form action="mail/index.php" method="post">
            <input type="hidden" name="invoiceid" value="' . $row["id"] . '" />
            <input type="hidden" name="receiver_email" value="' . $row["email"] . '" />
            <input type="hidden" name="receiver_name" value="' . $row["firstname"] . '" />
            <button type="submit" name="send_invoice_pdf"><i class="fa fa-mail-forward"></i></button>
         </form>
         </td>
         </tr>
      ';
      }
      $output .= '
         </tbody>
      </table>
      </div>
      ';
      return $output;
   }

   // Get all the clients
   function fetch_customer_data($connect)
   {
      $query = "SELECT * FROM members ORDER BY created_at DESC";
      $statement = $connect->prepare($query);
      $statement->execute();
      $result = $statement->fetchAll();
      $output = '
      <div class="table-responsive">
         <table class="table table-striped table-sm">
            <thead>
            <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Address</th>
                  <th>City</th>
            </tr>
            </thead>
            <tbody>
      ';
         foreach ($result as $row) {
            $output .= '
         <tr>
         <td>' . $row["id"] . '</td>
         <td><a href="../admin/profile.php?client=' . $row["id"] . '">' . $row["firstname"] . ' ' . $row["lastname"] . '</a></td>
         <td>' . $row["address"] . '</td>
         <td>' . $row["city"] . '</td>
         </tr>
      ';
         }
         $output .= '
         </tbody>
      </table>
      </div>
      ';
      return $output;
   }

   // Get single client details
   function fetch_customer_details($connect, $id)
    {
        $stmt = $connect->prepare("SELECT * FROM members WHERE id=:id");
        $stmt->execute(['id' => $id]); 
        while ($row = $stmt->fetch()) {
            echo "" .$row["firstname"].' '.$row["lastname"]. "";
        }
    }

    // Get all client's invoices
    function fetch_client_invoices($connect, $id) {
        $query = "SELECT * FROM invoices WHERE user_id='$id' ORDER BY created_at DESC";
        $statement = $connect->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll();
        $output = '
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Company</th>
                    <th>Due Date</th>
                    <th>Date Paid</th>
                    <th>Status</th>
                    <th>View</th>
                    <th>PDF</th>
                    <th>Reminder</th>
                </tr>
                </thead>
                <tbody>
        ';
        foreach($result as $row)
        {
        $output .= '
        <tr>
            <td>'.$row["invoice_no"].'</td>
            <td>'.$row["firstname"].' '.$row["lastname"].'</td>
            <td>'.$row["company"].'</td>
            <td>'.$row["due"].'</td>
            <td>'.$row["date_paid"].'</td>
            <td><a class="'.$row["status"].'">'.$row["status"].'</a></td>
            <td><a class="btn btn-sm btn-outline-info" href="../admin/invoice.php?invoice=' . $row["id"] . '"><i class="fa fa-eye" style="color:#222"></i></a></td>
            <td>
            <form action="pdf.php" method="post">
               <input type="hidden" name="invoiceid" value="' . $row["id"] . '" />
               <button class="btn btn-sm btn-outline-info" type="submit" name="single_invoice_pdf"><i class="fa fa-download" style="color:#222"></i></button>
            </form>
            </td>
            <td>
            <form action="mail/index.php" method="post">
               <input type="hidden" name="invoiceid" value="' . $row["id"] . '" />
               <input type="hidden" name="receiver_email" value="' . $row["email"] . '" />
               <input type="hidden" name="receiver_name" value="' . $row["firstname"] . '" />
               <button class="btn btn-sm btn-outline-info" type="submit" name="send_invoice_pdf"><i class="fa fa-mail-forward" style="color:#222"></i></button>
            </form>
            </td>
        </tr>
        ';
        }
        $output .= '
            </tbody>
        </table>
        </div>
        ';
        return $output;
    }

    // generate pdf for all invoices
   function send_invoices_data($connect)
   {
      $query = "SELECT * FROM invoices ORDER BY created_at DESC";
      $statement = $connect->prepare($query);
      $statement->execute();
      $result = $statement->fetchAll();
      $output = '
      <!DOCTYPE html>
      <html>
      <head>
      <style>
      .a {
         text-decoration: none;
         color: black;
      }
      .table {
         width: 100%;
         max-width: 100%;
         margin-bottom: 1rem;
         background-color: transparent;
      }
      
      .table th,
      .table td {
         padding: 0.75rem;
         vertical-align: top;
         border-top: 1px solid #dee2e6;
      }
      
      .table thead th {
         vertical-align: bottom;
         border-bottom: 2px solid #dee2e6;
      }
      
      .table tbody + tbody {
         border-top: 2px solid #dee2e6;
      }
      
      .table .table {
         background-color: #fff;
      }
      
      .table-sm th,
      .table-sm td {
         padding: 0.3rem;
      }
      
      .table-bordered {
         border: 1px solid #dee2e6;
      }
      
      .table-bordered th,
      .table-bordered td {
         border: 1px solid #dee2e6;
      }
      
      .table-bordered thead th,
      .table-bordered thead td {
         border-bottom-width: 2px;
      }
      
      .table-striped tbody tr:nth-of-type(odd) {
         background-color: rgba(0, 0, 0, 0.05);
      }
      
      .table-hover tbody tr:hover {
         background-color: rgba(0, 0, 0, 0.075);
      }
      
      .table-primary,
      .table-primary > th,
      .table-primary > td {
         background-color: #b8daff;
      }
      
      .table-hover .table-primary:hover {
         background-color: #9fcdff;
      }
      
      .table-hover .table-primary:hover > td,
      .table-hover .table-primary:hover > th {
         background-color: #9fcdff;
      }
      
      .table-secondary,
      .table-secondary > th,
      .table-secondary > td {
         background-color: #d6d8db;
      }
      
      .table-hover .table-secondary:hover {
         background-color: #c8cbcf;
      }
      
      .table-hover .table-secondary:hover > td,
      .table-hover .table-secondary:hover > th {
         background-color: #c8cbcf;
      }
      
      .table-success,
      .table-success > th,
      .table-success > td {
         background-color: #c3e6cb;
      }
      
      .table-hover .table-success:hover {
         background-color: #b1dfbb;
      }
      
      .table-hover .table-success:hover > td,
      .table-hover .table-success:hover > th {
         background-color: #b1dfbb;
      }
      
      .table-info,
      .table-info > th,
      .table-info > td {
         background-color: #bee5eb;
      }
      
      .table-hover .table-info:hover {
         background-color: #abdde5;
      }
      
      .table-hover .table-info:hover > td,
      .table-hover .table-info:hover > th {
         background-color: #abdde5;
      }
      
      .table-warning,
      .table-warning > th,
      .table-warning > td {
         background-color: #ffeeba;
      }
      
      .table-hover .table-warning:hover {
         background-color: #ffe8a1;
      }
      
      .table-hover .table-warning:hover > td,
      .table-hover .table-warning:hover > th {
         background-color: #ffe8a1;
      }
      
      .table-danger,
      .table-danger > th,
      .table-danger > td {
         background-color: #f5c6cb;
      }
      
      .table-hover .table-danger:hover {
         background-color: #f1b0b7;
      }
      
      .table-hover .table-danger:hover > td,
      .table-hover .table-danger:hover > th {
         background-color: #f1b0b7;
      }
      
      .table-light,
      .table-light > th,
      .table-light > td {
         background-color: #fdfdfe;
      }
      
      .table-hover .table-light:hover {
         background-color: #ececf6;
      }
      
      .table-hover .table-light:hover > td,
      .table-hover .table-light:hover > th {
         background-color: #ececf6;
      }
      
      .table-dark,
      .table-dark > th,
      .table-dark > td {
         background-color: #c6c8ca;
      }
      
      .table-hover .table-dark:hover {
         background-color: #b9bbbe;
      }
      
      .table-hover .table-dark:hover > td,
      .table-hover .table-dark:hover > th {
         background-color: #b9bbbe;
      }
      
      .table-active,
      .table-active > th,
      .table-active > td {
         background-color: rgba(0, 0, 0, 0.075);
      }
      
      .table-hover .table-active:hover {
         background-color: rgba(0, 0, 0, 0.075);
      }
      
      .table-hover .table-active:hover > td,
      .table-hover .table-active:hover > th {
         background-color: rgba(0, 0, 0, 0.075);
      }
      
      .table .thead-dark th {
         color: #fff;
         background-color: #212529;
         border-color: #32383e;
      }
      
      .table .thead-light th {
         color: #495057;
         background-color: #e9ecef;
         border-color: #dee2e6;
      }
      
      .table-dark {
         color: #fff;
         background-color: #212529;
      }
      
      .table-dark th,
      .table-dark td,
      .table-dark thead th {
         border-color: #32383e;
      }
      
      .table-dark.table-bordered {
         border: 0;
      }
      
      .table-dark.table-striped tbody tr:nth-of-type(odd) {
         background-color: rgba(255, 255, 255, 0.05);
      }
      
      .table-dark.table-hover tbody tr:hover {
         background-color: rgba(255, 255, 255, 0.075);
      }
      </style>
      </head>
      <body>
      <div class="table-responsive">
         <table class="table table-striped table-sm">
            <thead>
            <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Company</th>
                  <th>Due Date</th>
                  <th>Date Paid</th>
                  <th>Membership</th>
                  <th>Status</th>
            </tr>
            </thead>
            <tbody>
      ';
      foreach($result as $row)
      {
      $output .= '
         <tr>
         <td>'.$row["invoice_no"].'</td>
         <td><a href="../admin/profile.php?client=' . $row["user_id"] . '">' . $row["firstname"] . ' ' . $row["lastname"] . '</a></td>
         <td>'.$row["company"].'</td>
         <td>'.$row["due"].'</td>
         <td>'.$row["date_paid"].'</td>
         <td>'.$row["membership"].'</td>
         <td><p class='.$row["status"].'>'.$row["status"].'</p></td>
         </tr>
      ';
      }
      $output .= '
         </tbody>
      </table>
      </div>
      </body>
      </html>
      ';
      return $output;
   }

 // generate pdf for all invoices
 function fetch_customer_pdf($connect)
 {
    $query = "SELECT * FROM members ORDER BY created_at DESC";
    $statement = $connect->prepare($query);
    $statement->execute();
    $result = $statement->fetchAll();
    $output = '
    <!DOCTYPE html>
    <html>
    <head>
    <style>
    .a {
       text-decoration: none;
       color: black;
    }
    .table {
       width: 100%;
       max-width: 100%;
       margin-bottom: 1rem;
       background-color: transparent;
    }
    
    .table th,
    .table td {
       padding: 0.75rem;
       vertical-align: top;
       border-top: 1px solid #dee2e6;
    }
    
    .table thead th {
       vertical-align: bottom;
       border-bottom: 2px solid #dee2e6;
    }
    
    .table tbody + tbody {
       border-top: 2px solid #dee2e6;
    }
    
    .table .table {
       background-color: #fff;
    }
    
    .table-sm th,
    .table-sm td {
       padding: 0.3rem;
    }
    
    .table-bordered {
       border: 1px solid #dee2e6;
    }
    
    .table-bordered th,
    .table-bordered td {
       border: 1px solid #dee2e6;
    }
    
    .table-bordered thead th,
    .table-bordered thead td {
       border-bottom-width: 2px;
    }
    
    .table-striped tbody tr:nth-of-type(odd) {
       background-color: rgba(0, 0, 0, 0.05);
    }
    
    .table-hover tbody tr:hover {
       background-color: rgba(0, 0, 0, 0.075);
    }
    
    .table-primary,
    .table-primary > th,
    .table-primary > td {
       background-color: #b8daff;
    }
    
    .table-hover .table-primary:hover {
       background-color: #9fcdff;
    }
    
    .table-hover .table-primary:hover > td,
    .table-hover .table-primary:hover > th {
       background-color: #9fcdff;
    }
    
    .table-secondary,
    .table-secondary > th,
    .table-secondary > td {
       background-color: #d6d8db;
    }
    
    .table-hover .table-secondary:hover {
       background-color: #c8cbcf;
    }
    
    .table-hover .table-secondary:hover > td,
    .table-hover .table-secondary:hover > th {
       background-color: #c8cbcf;
    }
    
    .table-success,
    .table-success > th,
    .table-success > td {
       background-color: #c3e6cb;
    }
    
    .table-hover .table-success:hover {
       background-color: #b1dfbb;
    }
    
    .table-hover .table-success:hover > td,
    .table-hover .table-success:hover > th {
       background-color: #b1dfbb;
    }
    
    .table-info,
    .table-info > th,
    .table-info > td {
       background-color: #bee5eb;
    }
    
    .table-hover .table-info:hover {
       background-color: #abdde5;
    }
    
    .table-hover .table-info:hover > td,
    .table-hover .table-info:hover > th {
       background-color: #abdde5;
    }
    
    .table-warning,
    .table-warning > th,
    .table-warning > td {
       background-color: #ffeeba;
    }
    
    .table-hover .table-warning:hover {
       background-color: #ffe8a1;
    }
    
    .table-hover .table-warning:hover > td,
    .table-hover .table-warning:hover > th {
       background-color: #ffe8a1;
    }
    
    .table-danger,
    .table-danger > th,
    .table-danger > td {
       background-color: #f5c6cb;
    }
    
    .table-hover .table-danger:hover {
       background-color: #f1b0b7;
    }
    
    .table-hover .table-danger:hover > td,
    .table-hover .table-danger:hover > th {
       background-color: #f1b0b7;
    }
    
    .table-light,
    .table-light > th,
    .table-light > td {
       background-color: #fdfdfe;
    }
    
    .table-hover .table-light:hover {
       background-color: #ececf6;
    }
    
    .table-hover .table-light:hover > td,
    .table-hover .table-light:hover > th {
       background-color: #ececf6;
    }
    
    .table-dark,
    .table-dark > th,
    .table-dark > td {
       background-color: #c6c8ca;
    }
    
    .table-hover .table-dark:hover {
       background-color: #b9bbbe;
    }
    
    .table-hover .table-dark:hover > td,
    .table-hover .table-dark:hover > th {
       background-color: #b9bbbe;
    }
    
    .table-active,
    .table-active > th,
    .table-active > td {
       background-color: rgba(0, 0, 0, 0.075);
    }
    
    .table-hover .table-active:hover {
       background-color: rgba(0, 0, 0, 0.075);
    }
    
    .table-hover .table-active:hover > td,
    .table-hover .table-active:hover > th {
       background-color: rgba(0, 0, 0, 0.075);
    }
    
    .table .thead-dark th {
       color: #fff;
       background-color: #212529;
       border-color: #32383e;
    }
    
    .table .thead-light th {
       color: #495057;
       background-color: #e9ecef;
       border-color: #dee2e6;
    }
    
    .table-dark {
       color: #fff;
       background-color: #212529;
    }
    
    .table-dark th,
    .table-dark td,
    .table-dark thead th {
       border-color: #32383e;
    }
    
    .table-dark.table-bordered {
       border: 0;
    }
    
    .table-dark.table-striped tbody tr:nth-of-type(odd) {
       background-color: rgba(255, 255, 255, 0.05);
    }
    
    .table-dark.table-hover tbody tr:hover {
       background-color: rgba(255, 255, 255, 0.075);
    }
    </style>
    </head>
    <body>
    <div class="table-responsive">
    <table class="table table-striped table-sm">
       <thead>
       <tr>
             <th>#</th>
             <th>Name</th>
             <th>Address</th>
             <th>City</th>
       </tr>
       </thead>
       <tbody>
   ';
      foreach ($result as $row) {
         $output .= '
      <tr>
      <td>' . $row["id"] . '</td>
      <td><a href="../admin/profile.php?client=' . $row["id"] . '">' . $row["firstname"] . ' ' . $row["lastname"] . '</a></td>
      <td>' . $row["address"] . '</td>
      <td>' . $row["city"] . '</td>
      </tr>
   ';
      }
      $output .= '
         </tbody>
      </table>
      </div>
      </body>
      </html>
      ';
      return $output;
}

// Get invoice details in the invoice show page
function get_invoice_details($connect, $id)
{
     $stmt = $connect->prepare("SELECT * FROM invoices WHERE id=:id");
     $stmt->execute(['id' => $id]); 
     while ($row = $stmt->fetch()) {
      $newtime = strtotime($row["created_at"]);
      $stmt->time = date('Y-m-d',$newtime);
      $output = '
            <tr class="top">
            <td colspan="4">
               <table>
                  <tr>
                  <td class="title">
                     <img src="" class="img-fluid resize" alt="logo" />
                  </td>

                  <td>
                     Invoice #: <b>'.$row["invoice_no"].'</b>
                     <br>
                     Invoice Date: <b>'.$stmt->time.'</b>
                     <br>
                     Due: <b>'.$row["due"].'</b>
                     <br>
                     Status: <span class="'.$row["status"].'">'.$row["status"].'</span>
                  </td>
                  </tr>
               </table>
            </td>
            </tr>

            <tr class="information">
            <td colspan="4">
               <table>
                  <tr>
                  <td>
                     <b>Hekani</b><br>
                     1234 road harare<br>
                     <i class="fa fa-envelope"></i> example@mail.com<br>
                     <i class="fa fa-phone"></i> 07123456789
                  </td>

                  <td>
                     <b>Customer</b><br>
                     '.$row["address"].'<br>
                     <i class="far fa-envelope"></i> '.$row["email"].'<br>
                     <i class="fas fa-phone"></i> '.$row["phone"].'
                  </td>
                  </tr>
               </table>
            </td>
            </tr>

            <tr class="heading">
            <td class="pl-10">Payment Method</td>
            <td colspan="2"></td>
            <td class="center">Cache</td>
            </tr>

            <tr class="details">
            <td class="pl-10">Paynow Payment Gateway</td>
            <td colspan="2"></td>
            <td class="center"><button class="btn btn-sm btn-outline-secondary">Make Payment</button></td>
            </tr>

            <tr class="heading">
            <td class="pl-10">Item</td>
            <td colspan="1"></td>
            <td class="center">Qty</td>
            <td class="center">Amount ($)</td>
            </tr>
         ';
      return $output;
     }
}

// Get invoice items
function fetch_invoice_items($connect, $id) {
   $query = "SELECT * FROM invoiceitems WHERE invoiceid='$id'";
   $statement = $connect->prepare($query);
   $statement->execute();
   $result = $statement->fetchAll();
   $total = 0;
   $item_total = 0;
   $output = '

   ';
   foreach($result as $row)
   {
   $item_total = $row['quantity'] * $row['amount'];
   $total += $item_total;
   $output .= '
         <tr class="item">
            <td class="pl-10">
               '.$row["description"].'
            </td>

            <td colspan="1"></td>

            <td class="center">
               x '.$row["quantity"].'
            </td>

            <td class="center">
            '.$item_total.'
            </td>
         </tr>

         <tr>
         <td colspan="4"></td>
         </tr>
   ';
   }
   $output .= '
   <tr>
         <td colspan="2"></td>
         <td><b>Subtotal</b></td>
         <td class="center">'.$total.'</td>
         </tr>

         <tr>
         <td colspan="2"></td>
         <td><b>Discount 10%</b></td>
         <td class="center">- $0.00</td>
         </tr>

         <tr>
         <td colspan="2"></td>
         <td><b>VAT 10%</b></td>
         <td class="center">+ $0.00</td>
         </tr>

         <tr class="heading">
         <td colspan="2"></td>
         <td><b>TOTAL:</b></td>
         <td class="center">
            <b>$'.$total.'</b>
         </td>
        </tr>
   ';
   return $output;
}

?>