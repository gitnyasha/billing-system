<?php
   session_start();
   if(!isset($_SESSION['username'])){
     header('location:login.php');
   }
 
 
   if ( isset( $_GET[ "invoice" ] ) ) {
       $id = $_GET[ "invoice" ];
   }

   require('../admin/config.php');

   if (isset($_POST["add_item_action"])) {

      $stmt = $connect->prepare("SELECT * FROM `invoices` WHERE id=:id");
      $stmt->execute(['id' => $id]); 
      while ($row = $stmt->fetch()) {
              $description = $_POST["description"];
              $quantity = $_POST["quantity"];
              $amount = $_POST["amount"];
              $invoiceid = $row["id"];
      
              $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
              $stmt = $connect->prepare("INSERT INTO invoiceitems(
                  `description`, 
                  `quantity`, 
                  `amount`, 
                  `invoiceid`) VALUES(
                      '$description',
                      '$quantity',
                      '$amount',
                      '$invoiceid'
                  )");
                  if ($stmt->execute()) {
                      header("refresh: 1");
                      exit;
                  }
              $connect = null;
          }
    }

 ?>
      <!DOCTYPE html>
         <html lang="en">
         <head>
            <title>Invoice Design</title>
            <link rel="stylesheet" href="../css/invoice.css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="../css/bootstrap.min.css">
         </head>
         <body>
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col-lg-8 mx-auto p-4">
                     <a href="../admin" class="btn btn-outline-info"><i class="fa fa-mail-reply" style="color:#222"></i> Go Back</a>
                     <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#invoiceItemModal">
                     <i class="fa fa-plus">Add Items</i>
                     </button>
                  </div>
               </div>
            </div>
            <div class="invoice-box">
               <table cellpadding="0" cellspacing="0">
                  <?php 
                  echo get_invoice_details($connect, $id);
                  echo fetch_invoice_items($connect, $id);
                  ?>
               </table>
            </div>
         <!-- Add Items To Invoice Modal -->
         <div class="modal fade" id="invoiceItemModal" tabindex="-1" aria-labelledby="invoiceItemModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
               <div class="modal-header">
                     <h5 class="modal-title" id="invoiceItemModalLabel">
                     Invoice Item Form
                     </h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     </button>
               </div>
               <div class="modal-body">
               <form method="post">
                     <div class="row">
                        <div class="col-lg-6">
                           <label for="description">Description</label>
                           <input type="text" name="description" id="description" class="form-control" required >
                        </div>
                        <div class="col-lg-3">
                           <label for="quantity">Quantity</label>
                           <input type="number" name="quantity" class="form-control" value="1" id="quantity">
                           <small class="text-muted">Optional</small>
                        </div>
                        <div class="col-lg-3">
                           <label for="number">Amount</label>
                           <input type="text" name="amount" class="form-control" id="amount"  required>
                        </div>
                     </div>
                     <hr>
                     <input type="submit" name="add_item_action" class="btn btn-primary" value="Add To Invoice" />
                     </form>
               </div>
               
               </div>
            </div>
         </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
   </body>
</html>