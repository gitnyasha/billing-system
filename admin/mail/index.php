<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;
    use Dompdf\Dompdf;
    require '../../vendor/autoload.php';

    
    require_once __DIR__ . '/vendor/phpmailer/src/Exception.php';
    require_once __DIR__ . '/vendor/phpmailer/src/PHPMailer.php';
    require_once __DIR__ . '/vendor/phpmailer/src/SMTP.php';
    require('../config.php');

    // passing true in constructor enables exceptions in PHPMailer
    $mail = new PHPMailer(true);
    
    if ( isset( $_POST[ "invoiceid" ] ) ) {
        $id = $_REQUEST['invoiceid'];
    }
    
    if ( isset( $_POST[ "receiver_email" ] ) ) {
        $receiver_email = $_REQUEST['receiver_email'];
    }
    
    if ( isset( $_POST[ "receiver_name" ] ) ) {
        $receiver_name = $_REQUEST['receiver_name'];
    }
    
    if(isset($_POST["send_invoice_pdf"]))
    {


    $show = pdf_invoice_details($connect, $id);
    $dompdf = new Dompdf();
    $dompdf->loadHtml($show);
    $dompdf->setPaper('A4', 'landscape');
    $dompdf->render();
    $output = $dompdf->output();
    file_put_contents("invoice.pdf", $output);
    
    try {
        //Server settings
        // $mail->SMTPDebug = SMTP::DEBUG_SERVER;   
        // $mail->isSMTP();                              
        // $mail->Host       = 'smtp.example.com';    
        // $mail->SMTPAuth   = true;                   
        // $mail->Username   = 'user@example.com';     
        // $mail->Password   = 'secret';                       
        // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        // $mail->Port       = 465; 
       
        // Sender and recipient settings
        $mail->setFrom('example@mail.com', 'Sender Name');
        $mail->addAddress($receiver_email, $receiver_name);
        $mail->addReplyTo('example@mail.com', 'Sender Name');
        $mail->IsHTML(true);
        $mail->Subject = "Payment Invoice";
        $mail->Body = 'Please Find Invoice details in attached PDF File.';
        $mail->AddAttachment("invoice.pdf");
        $mail->AltBody = 'Please Find Invoice details in attached PDF File.';
        $mail->send();
        $html = '
        <!DOCTYPE html>
        <html lang="en">
         <head>
            <title>Confirmation</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
         </head>
         <body>
            <div class="container pt-5">
                <div class="row pt-5 justify-content-md-center">
                    <div class="col col-lg-2">
                    </div>
                    <div class="col-md-auto pt-5 text-center">
                    <h1>Email has been sent successfully</h1>
                    <a class="btn btn-success" href="../index.php">Go To Dashboard</a>
                    </div>
                    <div class="col col-lg-2">
                    </div>
                </div>
            </div>
         </body>
        </html>
        ';
        echo $html;
    } catch (Exception $e) {
        echo "Error in sending email. Mailer Error: {$mail->ErrorInfo}";
        echo "<a class='btn btn-success' href='../index.php'>Go To Dashboard</a>";
    }
    unlink("invoice.pdf");
}
?>
