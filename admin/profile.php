<?php
  session_start();
  if(!isset($_SESSION['username'])){
    header('location:login.php');
  }


  if ( isset( $_GET[ "client" ] ) ) {
      $id = $_GET[ "client" ];
  }

  require('../admin/config.php');
  
  if (isset($_POST["create_invoice_action"])) {

    $stmt = $connect->prepare("SELECT * FROM members WHERE id=:id");
    $stmt->execute(['id' => $id]); 
    while ($row = $stmt->fetch()) {
            $firstname = $row["firstname"];
            $lastname = $row["lastname"];
            $email = $row["email"];
            $address = $row["address"];
            $company = $row["company"];
            $due = $_POST["due"];
            $status = $_POST["status"];
            $date_paid = $_POST["datepaid"];
            $user_id = $row["id"];
    
            $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
            $stmt = $connect->prepare("INSERT INTO invoices(
                `invoice_no`,
                `firstname`, 
                `lastname`, 
                `email`, 
                `company`, 
                `address`, 
                `due`, 
                `status`, 
                `date_paid`, 
                `user_id`,
                `created_at`, 
                `updated_at`) VALUES(
                    '2',
                    '$firstname',
                    '$lastname',
                    '$email',
                    '$company',
                    '$address',
                    '$due',
                    '$status',
                    '$date_paid',
                    '$user_id',
                    NOW(),
                    NOW()
                )");
                if ($stmt->execute()) {
                    header("refresh: 1");
                    exit;
                }
            $connect = null;
        }
  }

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>Client</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">
  </head>

  <body>
  <?php include "nav.php" ?>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
             <?php include 'navigate.php'?>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
          <strong>Hello!</strong> To add items inside the invoice click the view button. 
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
            <h1 class="h2">
            <?php
            echo fetch_customer_details($connect, $id);
            ?>
            </h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-info">Generate PDF</button>
              </div>

              <button type="button" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#exampleModal">
                Create Invoice
              </button>
            </div>
          </div>

          <h2>Client Invoices</h2>
          <?php echo fetch_client_invoices($connect, $id) ?>
        </main>
      </div>
    </div>

    <!-- Create Invoice Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
            <?php
            echo fetch_customer_details($connect, $id);
            ?>
            Invoice Form
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        <form method="post">
            <div class="row">
                <div class="col-lg-6">
                    <label for="due">Due Date</label>
                    <input type="date" name="due" id="due" class="form-control" required >
                </div>
                <div class="col-lg-6">
                    <label for="datepaid">Date Paid</label>
                    <input type="date" name="datepaid" class="form-control" value="0000-00-00" id="date-paid">
                    <small class="text-muted">Optional</small>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-10">
                    <label for="status">Invoice Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="created">Created</option>
                        <option value="pending">Pending</option>
                        <option value="unpaid">unpaid</option>
                        <option value="paid">Paid</option>
                    </select>
                </div>
            </div>
            <hr>
            <input type="submit" name="create_invoice_action" class="btn btn-primary" value="Create Invoice" />
            </form>
        </div>
        
        </div>
    </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>
  </body>
</html>
